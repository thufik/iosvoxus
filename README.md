# README #

Esse README é referente ao Desafio da Voxus

### Resumo geral ###

Esse repositório é referente ao front-end do desafio que foi feita para iOS e escrito integralmente em swift. 

A aplicação possui 4 telas. A primeira delas é o login com o Google onde obtemos alguns dados do usuario. Após logar, o usuário tem 
a disposição uma lista com todas as tarefas cadastradas(Ou talvez ele devesse ver somente as tasks dele? :O)
A terceira tela é onde ele pode editar e salvar uma nova tarefa, algo simples, utilizando uma biblioteca gráfica que descobri 
recentemente (https://github.com/ra1028/Former). A última tela é uma simples tabela com o botão sair.

O front-end foi feito com base na Arquitetura Core, um framework para controle de estados da aplicação para iOS. Parecido com MVVM.
O autor explica o framework em detalhes [aqui](https://medium.com/nsistanbul/architecting-ios-apps-with-core-a7d769196b0a) e é algo que eu acredito muito.
Uma aplicação com um controle de estados assim dificilmente possui bugs de inconsistência de dados no front-end.
Eu já apliquei essa arquitetura em alguns projetos teste então não tive maiores problemas. 
O front-end mobile não foi demorado, terminei em umas 6 horas.


### Como executar ###

Para rodar o projeto é necessário o Xcode 9 ou posteriores. 

É necessário rodar o backend, senão não será possível acessar nenhuma funcionalidade ;)
