//
//  TaskModel.swift
//  DesafioVoxus
//
//  Created by Fellipe Thufik Costa Gomes Hoashi on 07/11/17.
//  Copyright © 2017 Fellipe Thufik Costa Gomes Hoashi. All rights reserved.
//

import ObjectMapper
import UIKit

public enum EnumTaskState : String{
    case sended = "sended"
    case processed = "processed"
    case done = "done"
}


class TaskModel: Mappable {

    var id : Int!
    var title : String!
    var description : String!
    var priority : Int!
    var submitter : String!
    var state : EnumTaskState!
    var user_id : Int?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        description <- map["description"]
        priority <- map["priority"]
        submitter <- map["submitter"]
        state <- map["state"]
        user_id <- map["user_id"]
    }
}
