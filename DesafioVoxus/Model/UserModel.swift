//
//  UserModel.swift
//  DesafioVoxus
//
//  Created by Fellipe Thufik Costa Gomes Hoashi on 12/11/17.
//  Copyright © 2017 Fellipe Thufik Costa Gomes Hoashi. All rights reserved.
//

import ObjectMapper
import UIKit


class UserModel: Mappable {

    var id : Int?
    var name : String?
    var email : String?
    var token : String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        email <- map["email"]
        token <- map["token"]
    }
}
