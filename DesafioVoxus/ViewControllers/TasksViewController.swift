//
//  ViewController.swift
//  DesafioVoxus
//
//  Created by Fellipe Thufik Costa Gomes Hoashi on 07/11/17.
//  Copyright © 2017 Fellipe Thufik Costa Gomes Hoashi. All rights reserved.
//

import CoreArchitecture
import FCAlertView
import JGProgressHUD
import SwipeCellKit
import UIKit

class tasksViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var tasks : [TaskModel] = []
    
    var task : TaskModel?

    let componentLoading = JGProgressHUD(style: .extraLight)

    let refreshControl = UIRefreshControl()

    var core : Core!

    var component : TaskListComponent!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addRefreshControlToTableView()
        self.setTableViewDelegates()
        self.setTableViewProperties()
        self.configCoreArchiteture()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.core.dispatch(TaskListAction.loadTasks)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func addRefreshControlToTableView(){
        self.refreshControl.addTarget(self, action: #selector(self.refreshTableView), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
    }

    private func setTableViewDelegates(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    private func setTableViewProperties(){
        self.tableView.allowsSelectionDuringEditing = true
        self.tableView.register(UINib(nibName : "TaskTableViewCell", bundle : nil), forCellReuseIdentifier: "taskCell")
    }
    
    private func configCoreArchiteture(){
        self.component = TaskListComponent(state: TaskListState(isLoading: false, hasError: nil, isEditing: false, tasksLoaded: nil, taskDeleted: false, taskDone: false, taskEditing: nil))
        
        self.core = Core(rootComponent: self.component)
        
        self.component.subscribe(self)
    }
    
    @IBAction func clickAddTaskButton(_ sender: UIBarButtonItem) {
        self.core.dispatch(TaskListAction.showCreateTasksView)
    }
    
    @objc private func refreshTableView(){
        self.core.dispatch(TaskListAction.loadTasks)
    }
    
    func showAddTaskView(component : addTaskComponent){
        
        let addTaskViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "addTaskViewController") as! addTaskViewController
        addTaskViewController.component = component
        
        let navigationController = UINavigationController(rootViewController: addTaskViewController)
        
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func showEditTaskView(component : addTaskComponent){
        
        let addTaskViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "addTaskViewController") as! addTaskViewController
        addTaskViewController.component = component
        
        if let task = self.task{
            addTaskViewController.task = task
        }
        
        self.navigationController?.pushViewController(addTaskViewController, animated: true)
    }
}

extension tasksViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "taskCell") as! TaskTableViewCell
        
        cell.delegate = self
        
        if let name = self.tasks[indexPath.row].title{
            cell.lblTitle.text = name
        }else{
            cell.lblTitle.text = "Não foi possível obter o nome"
        }
        
        if let state = self.tasks[indexPath.row].state{
            
            switch state{
                case .sended : cell.imgViewStatus.image = AppImage.sended.image
                case .processed: print("x")
                case .done: cell.imgViewStatus.image = AppImage.done.image
            }
            
        }
        
        cell.lblPriority.text = "Prioridade \(self.tasks[indexPath.row].priority!)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView.isEditing{
            let task = self.tasks[indexPath.row]
            self.core.dispatch(TaskListAction.showEditTaskView(model: task))
        }
    }
}

extension tasksViewController : SwipeTableViewCellDelegate{
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        
        let deleteAction = SwipeAction(style: .destructive, title: "Delete") { action, indexPath in
            
            let alert = MessageFactory(viewController: self)
            
            let task = self.tasks[indexPath.row]
            
            alert.showAlertDeleteTask(doneBlock: {
                self.core.dispatch(TaskListAction.deleteTask(id: task.id))
            })
        }
        
        let editAction = SwipeAction(style: .default, title: "Edit", handler: { action, indexPath in
            
            let task = self.tasks[indexPath.row]
            
            self.core.dispatch(TaskListAction.showEditTaskView(model: task))
        })
        
        let doneAction = SwipeAction(style: .default, title: "Done", handler: { action, indexPath in
            
            let task = self.tasks[indexPath.row]
            
            let alert = MessageFactory(viewController: self)
            
            alert.showAlertDoneTask(doneBlock: {
                self.core.dispatch(TaskListAction.doneTask(id: task.id))
            })
        })
        
        doneAction.backgroundColor = Colors.successColor.color
        
        return [deleteAction, doneAction, editAction]
        
    }
}

extension tasksViewController : Subscriber{
    typealias StateType = TaskListState
    
    func perform(_ navigation: Navigation) {
        
        guard let navigation = navigation as? BasicNavigation else{ return }
    
        switch navigation {
        case .present(let addTaskComponent, from: _):
            self.showAddTaskView(component:  addTaskComponent as! addTaskComponent)
        case .push(let addTaskComponent, from: let taskListComponent): self.showEditTaskView(component: addTaskComponent as! addTaskComponent)
        case .pop(_):
            return
        case .dismiss(_):
            return
        }
    }
    
    func update(with state: tasksViewController.StateType) {
        
        if state.isLoading{
            self.componentLoading.show(in: self.view, animated: true)
        }else{
            self.refreshControl.endRefreshing()
            self.componentLoading.dismiss()
        }
        
        if let error = state.hasError{
            let alert = MessageFactory(viewController: self)
            let error = NSError(domain: Messages.serverError.message, code: 500, userInfo: nil)
            
            alert.showGenericError(error: error)
        }
        
        if let task = state.taskEditing{
            self.task = task
        }
        
        if let taks = state.tasksLoaded{
            self.tasks = taks
            self.tableView.reloadData()
        }
        
        if state.taskDeleted || state.taskDone{
            self.core.dispatch(TaskListAction.loadTasks)
        }
        
        if state.isEditing{
            self.tableView.isEditing = true
        }else{
            self.tableView.isEditing = false
        }
    }
}
