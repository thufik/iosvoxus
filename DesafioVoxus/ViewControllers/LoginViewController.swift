//
//  LoginViewController.swift
//  DesafioVoxus
//
//  Created by Fellipe Thufik Costa Gomes Hoashi on 12/11/17.
//  Copyright © 2017 Fellipe Thufik Costa Gomes Hoashi. All rights reserved.
//

import CoreArchitecture
import FCAlertView
import JGProgressHUD
import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var btnGoogleSignIn: GIDSignInButton!
    var loginComponent : LoginComponent!
    var core : Core!
    let componentLoading = JGProgressHUD(style: .extraLight)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setGoogleSignInDelegates()
        self.setCoreArchiteture()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setCoreArchiteture(){
        self.loginComponent = LoginComponent(state: LoginState(isLoading: false, hasError: nil))
        self.loginComponent.subscribe(self)
        self.core = Core(rootComponent: self.loginComponent)
    }
    
    private func setGoogleSignInDelegates(){
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    func showTasksTableView(taskComponent : TaskListComponent){
        let tasksNavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabBar")

        if let tasksViewController = tasksNavigationController.childViewControllers.first as? tasksViewController{
            tasksViewController.component = taskComponent
        }
        
        (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = tasksNavigationController
        (UIApplication.shared.delegate as! AppDelegate).window?.makeKeyAndVisible()
    }
}

extension LoginViewController : GIDSignInUIDelegate{}

extension LoginViewController : GIDSignInDelegate{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            
//            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let email = user.profile.email
            
            var params : [String : Any] = [:]
            
            if let token = idToken{
                
                params["token"] = token
                
                if let fullName = fullName{
                    params["name"] = fullName
                }
                
                if let email = email{
                    params["email"] = email
                }
                
                self.core.dispatch(LoginAction.login(params: params))
            }
        } else {
            
        }
    }
}

extension LoginViewController : Subscriber{
    
    typealias StateType = LoginState
    
    func perform(_ navigation: Navigation) {
        
        guard let navigation = navigation as? BasicNavigation else { return }
        
        switch navigation {
        case .present(let tasksComponent, from: let _):
            self.showTasksTableView(taskComponent: tasksComponent as! TaskListComponent)
        default: print("teste")
        }
    }
    
    func update(with state: LoginViewController.StateType) {
        
        if state.isLoading{
            self.componentLoading.show(in: self.view, animated: true)
        }else{
            self.componentLoading.dismiss()
        }
        
        if let error = state.hasError{
            
            GIDSignIn.sharedInstance().signOut()
            
            let alert = FCAlertView()
            alert.blurBackground = false
            alert.alertBackgroundColor = UIColor.white
            alert.avoidCustomImageTint = true
            
            alert.showAlert(inView: self, withTitle: "Atenção", withSubtitle: error.domain, withCustomImage: UIImage(named : "voxusBalloon")!, withDoneButtonTitle: "Ok", andButtons: nil)
        }
    }
}
