//
//  addTaskViewController.swift
//  DesafioVoxus
//
//  Created by Fellipe Thufik Costa Gomes Hoashi on 07/11/17.
//  Copyright © 2017 Fellipe Thufik Costa Gomes Hoashi. All rights reserved.
//

import CoreArchitecture
import FCAlertView
import Former
import JGProgressHUD
import UIKit

class addTaskViewController: FormViewController {
    
    var taskNameRow : TextFieldRowFormer<FormTextFieldCell>!
    
    var taskDescriptionRow : TextViewRowFormer<FormTextViewCell>!
    
    var priorityPickerRow : InlinePickerRowFormer<FormInlinePickerCell, Int>!
    
    let componentLoading = JGProgressHUD(style: .extraLight)
    
    let refreshControl = UIRefreshControl()
    
    var core : Core!
    
    var component : addTaskComponent!
    
    var task : TaskModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureCells()
        self.configCoreArchiteture()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc private func clickCancelButton(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func clickSaveButton(){
        
        let priority = self.priorityPickerRow.selectedRow + 1
        
        var json = ["title" : self.taskNameRow.text!, "description" : self.taskDescriptionRow.text!, "priority" : priority, "user_id" : UserDefaults.standard.object(forKey: "id") as! Int] as [String : Any]
        
        if let task = self.task{
            json["state"] = EnumTaskState.sended.rawValue
            json["id"] = task.id!
            guard let updateTaskModel = TaskModel(JSON: json) else{ return }
            self.core.dispatch(addTaskAction.updateTask(task: updateTaskModel))
        }else{
            json["state"] = EnumTaskState.sended.rawValue
            guard let saveTaskModel = TaskModel(JSON: json) else{ return }
            self.core.dispatch(addTaskAction.addTask(task: saveTaskModel))
        }
    }

    private func configCoreArchiteture(){
        self.core = Core(rootComponent: self.component)
        self.component.subscribe(self)
    }
    
    func configureCancelButton(){
        let CancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.clickCancelButton))
        self.navigationController?.topViewController?.navigationItem.leftBarButtonItem = CancelButton
    }
    
    func configureSaveButton(){
        let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(self.clickSaveButton))
        self.navigationController?.topViewController?.navigationItem.rightBarButtonItem = saveButton
    }
    
    func clearAllFields(){
        self.taskNameRow.cell.textField.text = nil
        self.taskDescriptionRow.cell.textView.text = ""
        self.priorityPickerRow.selectedRow = 0
    }
    
    private func configureCells(){
        self.taskNameRow = TextFieldRowFormer<FormTextFieldCell>(){
            $0.titleLabel.text = "Digite o nome da task"
            $0.titleLabel.textColor = UIColor.gray
        }

        self.taskDescriptionRow = TextViewRowFormer<FormTextViewCell>(){
            $0.titleLabel.text = "Digite a descrição da task"
            $0.titleLabel.textColor = UIColor.gray
        }
        
        
        self.priorityPickerRow = InlinePickerRowFormer<FormInlinePickerCell, Int>() {
            $0.titleLabel.text = "Escolha a prioridade da task"
            $0.titleLabel.textColor = UIColor.gray
            }.configure { row in
                row.pickerItems = (1...5).map {
                    InlinePickerItem(title: "\($0)", value: Int($0))
                }
            }.onValueChanged { item in
                // Do Something
        }

        let section = SectionFormer(rowFormer: taskNameRow, taskDescriptionRow,priorityPickerRow)
        former.append(sectionFormer: section)
    }
}

extension addTaskViewController : Subscriber{
    typealias StateType = addTaskState
    
    func update(with state: addTaskViewController.StateType) {
        
        if state.isLoading{
            self.componentLoading.show(in: self.view, animated: true)
        }else{
            self.componentLoading.dismiss()
        }
        
        if state.isEditing == false{
            self.configureCancelButton()
            self.configureSaveButton()
        }else{
            if let task = self.task{
                self.taskNameRow.text = task.title!
                self.taskDescriptionRow.text = task.description!
                self.priorityPickerRow.selectedRow = task.priority - 1
            }
            self.configureSaveButton()
        }
        
        if let error = state.hasError{
            let alert = MessageFactory(viewController: self)

            let error = NSError(domain: Messages.serverError.message, code: 500, userInfo: nil)
            
            alert.showGenericError(error: error)
        }
        
        if let task = state.taskLoaded{
            let alert = MessageFactory(viewController: self)
            
            alert.showGenericSuccess {
                self.clearAllFields()
            }
        }
    }
    
    func perform(_ navigation: Navigation) {
        
    }
}
