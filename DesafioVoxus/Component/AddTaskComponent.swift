//
//  AddTaskComponent.swift
//  DesafioVoxus
//
//  Created by Fellipe Thufik Costa Gomes Hoashi on 07/11/17.
//  Copyright © 2017 Fellipe Thufik Costa Gomes Hoashi. All rights reserved.
//

import Alamofire
import CoreArchitecture

struct addTaskState : State{
    var isLoading : Bool
    var hasError : NSError?
    var isEditing : Bool
    var taskLoaded : TaskModel?
}

enum addTaskAction : Action{
    case addTask(task : TaskModel)
    case updateTask(task : TaskModel)
}

class addTaskComponent: Component<addTaskState> {
    
    override func process(_ action: Action) {
        
        guard let action = action as? addTaskAction else{ return }
        
        switch action {
            case .addTask(let task) : self.addTask(task: task)
            case .updateTask(task: let task) : self.updateTask(task: task)
        }
    }
    
    private func addTask(task : TaskModel){
        var state = self.state
        state.isLoading = true
        state.hasError = nil
        
        self.commit(state)
        
        Alamofire.request("https://backendvoxus.herokuapp.com/task", method: .post, parameters: task.toJSON(), encoding: JSONEncoding.default, headers: nil).responseJSON(completionHandler: { response in
            
            state.isLoading = false
            
            if let value = response.result.value{
                
                let task = value as? [String : Any]
                
                state.taskLoaded = TaskModel(JSON: task!)
                self.commit(state)
            }else{
                let error = NSError(domain: "Deu ruim", code: 100, userInfo: nil)
                state.hasError = error
                self.commit(state)
            }
        })
    }
    
    private func updateTask(task : TaskModel){
        var state = self.state
        state.isLoading = true
        state.hasError = nil
        
        self.commit(state)
        
        Alamofire.request("https://backendvoxus.herokuapp.com/task/\(task.id!)", method: .put, parameters: task.toJSON(), encoding: JSONEncoding.default, headers: nil).responseJSON(completionHandler: { response in
            
            state.isLoading = false
            
            if let value = response.result.value{
                
                let task = value as? [String : Any]
                
                state.taskLoaded = TaskModel(JSON: task!)
                self.commit(state)
            }else{
                let error = NSError(domain: "Deu ruim", code: 100, userInfo: nil)
                state.hasError = error
                self.commit(state)
            }
        })
    }
}
