//
//  LoginComponent.swift
//  DesafioVoxus
//
//  Created by Fellipe Thufik Costa Gomes Hoashi on 12/11/17.
//  Copyright © 2017 Fellipe Thufik Costa Gomes Hoashi. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import CoreArchitecture
import UIKit

enum LoginAction : Action {
    case login(params : [String : Any])
}

struct LoginState : State {
    var isLoading : Bool
    var hasError : NSError?
}

class LoginComponent: Component<LoginState> {

    override func process(_ action: Action) {
        guard let action = action as? LoginAction else { return }
        
        switch action {
            case .login(params: let params): self.login(params: params)
        }
    }
    
    private func login(params : [String : Any]){
        
        var state = self.state
        
        state.isLoading = true
        state.hasError = nil
        
        self.commit(state)
        
        Alamofire.request("https://backendvoxus.herokuapp.com/login", method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseObject(completionHandler: { (response : DataResponse<UserModel>) in
            
            state.isLoading = false
            
            if let user = response.result.value{
                
                let navigation = BasicNavigation.present(TaskListComponent(state : TaskListState(isLoading: false, hasError: nil, isEditing: false, tasksLoaded: nil, taskDeleted: false, taskDone: false, taskEditing: nil)), from: self)
                
                UserDefaults.standard.set(user.id!, forKey: "id")
                
                self.commit(state, navigation)
            }else{
                let error = NSError(domain: "deu ruim", code: 100, userInfo: nil)
                
                state.hasError = error
                self.commit(state)
            }
        })
        
    }
}
