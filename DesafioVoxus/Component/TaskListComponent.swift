//
//  TaskListComponent.swift
//  DesafioVoxus
//
//  Created by Fellipe Thufik Costa Gomes Hoashi on 07/11/17.
//  Copyright © 2017 Fellipe Thufik Costa Gomes Hoashi. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import CoreArchitecture

struct TaskListState : State{
    var isLoading : Bool
    var hasError : NSError?
    var isEditing : Bool
    var tasksLoaded : [TaskModel]?
    var taskDeleted : Bool
    var taskDone : Bool
    var taskEditing : TaskModel?
}

enum TaskListAction : Action{
    case showCreateTasksView
    case showEditTaskView(model : TaskModel)
    case loadTasks
    case deleteTask(id : Int)
    case doneTask(id : Int)
}

class TaskListComponent: Component<TaskListState> {
    
    override func process(_ action: Action) {
        
        guard let action = action as? TaskListAction else{ return }
        
        switch action {
            case .showCreateTasksView : self.showTasksView()
            case .showEditTaskView(let task) : self.showEditTaskView(task : task)
            case .loadTasks : self.loadTasks()
            case .deleteTask(id: let id) : self.deleteTask(id : id)
            case .doneTask(id: let id) : self.doneTask(id: id)
        }
    }
    
    private func doneTask(id : Int){
        var state = self.state
        
        state.hasError = nil
        state.isLoading = true
        state.taskDeleted = false
        state.taskDone = false
        
        self.commit(state)
        
        Alamofire.request("https://backendvoxus.herokuapp.com/task/\(id)/done", method: .put, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON(completionHandler: { response in
            
            state.isLoading = false
            
            if (response.result.value) != nil{
                state.taskDone = true
                
                self.commit(state)
            }else{
                let error = NSError(domain: "Deu ruim", code: 100, userInfo: nil)
                
                state.hasError = error
                self.commit(state)
            }
            
        })
    }
    
    private func deleteTask(id : Int){
        var state = self.state
        
        state.hasError = nil
        state.isLoading = true
        state.taskDeleted = false
        state.taskDone = false
        
        self.commit(state)
        
        Alamofire.request("https://backendvoxus.herokuapp.com/task/\(id)", method: .delete, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON(completionHandler: { response in
            
                state.isLoading = false
            
                if let result = response.result.value{
                    
                    state.taskDeleted = true
                    
                    self.commit(state)
                }else{
                    let error = NSError(domain: "Deu ruim", code: 100, userInfo: nil)
                    
                    state.hasError = error
                    self.commit(state)
                }
        })
    }
    
    private func showTasksView(){
        let navigation = BasicNavigation.present(addTaskComponent(state: addTaskState(isLoading: false, hasError: nil, isEditing: false, taskLoaded: nil)), from:  self)
        
        self.commit(navigation)
    }
    
    private func showEditTaskView(task : TaskModel){
        let navigation = BasicNavigation.push(addTaskComponent(state: addTaskState(isLoading: false, hasError: nil, isEditing: true, taskLoaded: nil)), from: self)
        
        var state = self.state
        state.taskEditing = task
        
        self.commit(state, navigation)
    }
    
    private func loadTasks(){
        
        var state = self.state
        
        state.hasError = nil
        state.isLoading = true
        state.taskDeleted = false
        state.taskDone = false
        
        self.commit(state)
        
        Alamofire.request("https://backendvoxus.herokuapp.com/tasks", method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseArray(completionHandler: { (response : DataResponse<[TaskModel]>) in
            
            state.isLoading = false
            
            if let result = response.result.value{
                
                state.tasksLoaded = result
                
                self.commit(state)
            }else{
                let error = NSError(domain: "Deu ruim", code: 100, userInfo: nil)
                
                state.hasError = error
                self.commit(state)
            }
        })
    }
}
