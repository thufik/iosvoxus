//
//  Messages.swift
//  DesafioVoxus
//
//  Created by Fellipe Thufik Costa Gomes Hoashi on 17/11/17.
//  Copyright © 2017 Fellipe Thufik Costa Gomes Hoashi. All rights reserved.
//

import UIKit

public enum Messages{
    case serverError
    case taskSaved
    
    var message : String{
        switch self {
            case .serverError: return "Houve um erro inesperado"
            case .taskSaved : return "Tarefa salva"
        }
    }
}
