//
//  Colors.swift
//  DesafioVoxus
//
//  Created by Fellipe Thufik Costa Gomes Hoashi on 17/11/17.
//  Copyright © 2017 Fellipe Thufik Costa Gomes Hoashi. All rights reserved.
//

import UIKit

public enum Colors{
    
    case voxusPurple
    case successColor
    
    var color : UIColor{
        switch self {
            case .voxusPurple : return UIColor(red: 74.0/255.0, green: 41.0/255.0, blue: 62.0/255.0, alpha: 1.0)
            case .successColor : return UIColor(red: 47.0/255.0, green: 133.0/255.0, blue: 24.0/255.0, alpha: 1.0)
        }
    }
}
