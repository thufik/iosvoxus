//
//  MessageFactory.swift
//  DesafioVoxus
//
//  Created by Fellipe Thufik Costa Gomes Hoashi on 16/11/17.
//  Copyright © 2017 Fellipe Thufik Costa Gomes Hoashi. All rights reserved.
//

import FCAlertView
import UIKit

class MessageFactory: NSObject {

    let alert = FCAlertView()
    var viewController : UIViewController!
    
    init(viewController : UIViewController){
        self.viewController = viewController
        self.alert.blurBackground = false
        self.alert.alertBackgroundColor = UIColor.white
        self.alert.avoidCustomImageTint = true
    }
    
    public func showAlertDeleteTask(doneBlock : @escaping () -> Void){
        self.alert.showAlert(inView: self.viewController, withTitle: "Atenção", withSubtitle: "Tem certeza que deseja deletar essa tarefa?", withCustomImage: AppImage.baloon.image, withDoneButtonTitle: nil, andButtons: nil)
        
        self.alert.hideDoneButton = true
        self.alert.addButton("Sim", withActionBlock: {
            doneBlock()
        })
        
        self.alert.addButton("Não", withActionBlock: nil)
        self.alert.firstButtonTitleColor = Colors.voxusPurple.color
        self.alert.secondButtonTitleColor = UIColor.white
        self.alert.secondButtonBackgroundColor = Colors.voxusPurple.color
    }
    
    public func showAlertDoneTask(doneBlock : @escaping () -> Void){
        self.alert.showAlert(inView: self.viewController, withTitle: "Atenção", withSubtitle: "Deseja concluir essa tarefa?", withCustomImage: AppImage.baloon.image, withDoneButtonTitle: nil, andButtons: nil)
        
        self.alert.addButton("Sim", withActionBlock: {
            doneBlock()
        })
        
        self.alert.hideDoneButton = true
        self.alert.addButton("Não", withActionBlock: nil)
        self.alert.firstButtonTitleColor = Colors.voxusPurple.color
        self.alert.secondButtonTitleColor = UIColor.white
        self.alert.secondButtonBackgroundColor = Colors.voxusPurple.color
    }
    
    public func showAlertLogout(){
        self.alert.addButton("Sim", withActionBlock: {
            GIDSignIn.sharedInstance().signOut()
            let loginViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginViewController")
            
            UserDefaults.standard.removeObject(forKey: "user_id")
            (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = loginViewController
        })
        
        self.alert.hideDoneButton = true
        self.alert.addButton("Não", withActionBlock: nil)
        self.alert.firstButtonTitleColor = Colors.voxusPurple.color
        self.alert.secondButtonTitleColor = UIColor.white
        self.alert.secondButtonBackgroundColor = Colors.voxusPurple.color
        
        self.alert.showAlert(withTitle: "Atenção", withSubtitle: "Tem certeza que deseja sair?", withCustomImage: AppImage.baloon.image, withDoneButtonTitle: nil, andButtons: nil)
    }
    
    public func showGenericError(error : NSError){
        self.alert.hideDoneButton = true
        self.alert.addButton("Ok", withActionBlock: nil)
        self.alert.firstButtonTitleColor = UIColor.white
        self.alert.firstButtonBackgroundColor = Colors.voxusPurple.color
        
        self.alert.showAlert(withTitle: "Atenção", withSubtitle: error.domain, withCustomImage: AppImage.baloon.image, withDoneButtonTitle: nil, andButtons: nil)
    }
    
    public func showGenericSuccess(doneBlock : @escaping () -> Void){
        self.alert.hideDoneButton = true
        self.alert.addButton("Ok", withActionBlock: {
            doneBlock()
        })
        
        self.alert.firstButtonTitleColor = UIColor.white
        self.alert.firstButtonBackgroundColor = Colors.voxusPurple.color
        
        self.alert.showAlert(withTitle: "Sucesso", withSubtitle: "Tarefa salva", withCustomImage: AppImage.baloon.image, withDoneButtonTitle: nil, andButtons: nil)
    }
}
