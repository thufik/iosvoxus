//
//  Images.swift
//  DesafioVoxus
//
//  Created by Fellipe Thufik Costa Gomes Hoashi on 16/11/17.
//  Copyright © 2017 Fellipe Thufik Costa Gomes Hoashi. All rights reserved.
//

import UIKit

public enum AppImage{
    case baloon
    case done
    case sended
    
    var image : UIImage{
        switch self {
            case .baloon:
                return UIImage(named : "voxusBalloon")!
            case .done:
                return UIImage(named : "doneImage")!
            case .sended:
                return UIImage(named : "sendedImage")!
        }
    }
}
