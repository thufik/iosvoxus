//
//  TaskTableViewCell.swift
//  DesafioVoxus
//
//  Created by Fellipe Thufik Costa Gomes Hoashi on 12/11/17.
//  Copyright © 2017 Fellipe Thufik Costa Gomes Hoashi. All rights reserved.
//

import SwipeCellKit
import UIKit

class TaskTableViewCell: SwipeTableViewCell {

    @IBOutlet weak var imgViewStatus: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblPriority: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
