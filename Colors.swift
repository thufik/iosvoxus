//
//  Colors.swift
//  DesafioVoxus
//
//  Created by Fellipe Thufik Costa Gomes Hoashi on 17/11/17.
//  Copyright © 2017 Fellipe Thufik Costa Gomes Hoashi. All rights reserved.
//

import UIKit

public enum Colors {
    
    case voxusPurple
    
    var color : UIColor{
        switch self {
            case .voxusPurple : return UIColor(red: 74.0/255.0, green: 41.0/255.0, blue: 62.0/255.0, alpha: 1.0)
        }
    }
}
